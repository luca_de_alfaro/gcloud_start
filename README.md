# gcloud_start

These instructions will allow you to upload your py4web app to the Google cloud, 
on Google Appengine for Python 3, with a MySQL database. 
This will give you a very scalable, very easy way to serve your web application. 

## Prepare the Google Cloud hosting environment

### Create a Google Cloud Project

Go to https://cloud.console.google.com, and create a new project.  See the [documentation on creating projects](https://cloud.google.com/resource-manager/docs/creating-managing-projects) for help.  You can create the project from https://console.cloud.google.com/cloud-resource-manager if such an option is not offered on the top bar. 

### Setup billing

If you wish to use a SQL database, or Google Cloud Storage, or many other services, you need to set up a billing account connected to the project.  Do so at the [billing page](https://console.cloud.google.com/billing), then go to the [dashboard](https://console.cloud.google.com/home/dashboard), selecti billing, and add the billing account to the project. 

### Install the gcloud tools

Follow the instructions at https://cloud.google.com/sdk/docs/downloads-interactive to install the gcloud tools on your laptop / PC. 

### Create a configuration

Gcloud tools act on projects.  It is a good idea to create a _configuration_, which will store the project on which we want to operate.  We can then activate this configuration when we want to work on our project, similarly to what is done via Python environments. 

To create a configuration, do: 

    gcloud config configuration create myconfig
    
where obviously you should replace `myconfig` with what you want. 
Then you can add basic configuration parameters to myconfig, among which: 

    gcloud config set project yourproject
    gcloud config set account your_account@gmail.com
    gcloud config set gcloud config set app/promote_by_default false
    
The later setting means that when you upload an app to google appengine, it does not immediately become active.  You will need to activate it manually.  This is basically a security setting. 

You can activate this configuration later via 

    gcloud config configurations activate myconfig

## Create an SQL server

### Background

On the Google cloud, there are many solutions to store your data; here is an [overview](https://cloud.google.com/products/storage). 
A simiplification is as follows: 

* Cloud SQL is a managed SQL database server; it can use mysql or postgres.  Scalable up to decent but not extreme loads.  Managed, you can add replicas, daily backups, etc.  Cost from about $10/month for development servers to $50/month for production ones (and more, for more server capacity and more storage).
* Firestore is... likely a fancy database thought as backend for mobile apps and the like, but thankfully it can be used in Datastore mode. 
* Datastore (or firestore in datastore mode) is a very scalable no-SQL database.  No minimum price, you pay per operation and per data stored.  Can be more expensive than SQL if number of operations/month is high. 
* Spanner is a fanstatically scaling SQL database, with global consistency and all.  It can be expensive, and has a high minimum price, but if you need scaling with consistency, look no further. 
* Cloud Bigtable... well, it's like Datastore but less good and more complex to configure (go figure).
* Cloud Storage is used for blobs; it's not a database. 

Py4web, via PyDal, works with a very large number of databases, see the [PyDAL documentation](http://www.web2py.com/books/default/chapter/29/06/the-database-abstraction-layer#Connection-strings-the-uri-parameter-). 
However, the Google datastore databases, that is, the prefixes startign with google:, have not been ported yet to appengine Python 3.  
In particular: 

* Google/SQL is what we will use, but we will need a different connection string. 
* Google/NoSQL and Google/NoSQL/NDB: the adaptation is still in progress.

### Create an SQL server

In practice, this means that you will have to create an SQL server. 
Go to your project, select "Cloud SQL" on the left bar, and create an SQL server, using mysql for instance. 

* Choose an instance size.  Note that an instance can serve multiple databases, so if you have multiple projects, you can use one database server for them all, unless they require a lot of resources. 
* Choose an instance name, and take care to write down and choose a good root password. 
* Choose a region close to where you want to use it. 
* You can leave most of the other options unchanged. 

If you are given a choice at all, remember to use flash memory (SSD), not a normal hard drive, if you care about performance. 

Once this is done, you need to create a database, a user for this database, and 
you need to grant permissions to this user for the database. 

### Create the databases

Click on Databases, and create two database.  

* A testing database
* A production database

Give them names composed of only letters and dashes, that is related to the application you want to serve.  
See the [SQL identifier rules](https://dev.mysql.com/doc/refman/5.7/en/identifiers.html).  Keep it simple. 

**Important**: for the character set, use `utf8mb4`.  The default setting for mysql, `utf8`, does not recognize the complete set of UTF characters. 

### Create database users

Click on Users, and create two users, with their passwords.
You will use one user for the testing database, and one for the production database. 
If you create users via the cloud console, they still will have access to all databases.
You can tailor their access via, for instance (see the [documentation for grant](https://dev.mysql.com/doc/refman/5.7/en/grant.html)):

    REVOKE ALL PRIVILEGES, GRANT OPTION FROM test_user;
    FLUSH PRIVILEGES;
    GRANT ALL on test_db.* to test_user;
    FLUSH PRIVILEGES;

### Configure the SQL server connection strings

We are going to need two connection strings, one for the testing database, one for the production one. 
In our simple setup, we will use the condition:

    if os.environ.get("GAE_ENV"):

to decide which string to use.  Thus, we use the presence of the environment variable GAE_ENV to check whether we are on Appengine in production, or on localhost in testing.  See the description of the [Appengine Python runtime environment](https://cloud.google.com/appengine/docs/standard/python3/runtime) for these variables. 
Thus, we are going to configure:
    
* `GAE_DB_URI` for production use;
* `TESTING_DB_URI` for development use. 

#### Testing database connection string

`TESTING_DB_URI` can be as simple as 

    TESTING_DB_URI = "sqlite://storage.db"

if we want to test using SQLite locally, but if we wish to use a cloud mysql database, as advised, then the URL will have the form: 

    CLOUD_DB_URI = "mysql://username:password@IP/test_db?set_encoding=utf8mb4"
    
where `IP` is the IP address of the server. 
Of coruse, in this latter case, put the URL in `settings_private.py` in order to avoid committing passwords to git logs. 
You can find that IP address in the connections page for the Cloud SQL server. 
Note that, to be able to connect, you also have to whitelist your local IP address, as only some IP addresses can connect to the cloud. 

Another possibe solution is to use, for testing, an installation of MySQL local to your laptop.  You can install MySQL on it following these [installation instructions](https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/). 

The DAL URI is then: 

    db = DAL(settings.TESTING_DB_URI,
             folder=settings.DB_FOLDER,
             pool_size=settings.DB_POOL_SIZE)

#### Production database connection string

The `GAE_DB_URI` is generated as follows. 
Visit the Cloud SQL page on your google cloud console, click on the particular instance, and look at the connection name, which will have a form such as: 

    <project_name>:<zone>:<instance_name>
    
Then, in `settings_private.py`, define:

    DB_USER = "production_user"
    DB_PASSWORD = "production password"
    DB_NAME = "production_db"
    DB_CONNECTION = "<project_name>:<zone>:<instance_name>"

where obviously you have to substitute the correct values. 
The connection string is:

    GAE_DB_URI = "google:MySQLdb://{DB_USER}:{DB_PASSWORD}@/{DB_NAME}?unix_socket=/cloudsql/{DB_CONNECTION}".format(
        DB_USER=DB_USER,
        DB_NAME=DB_NAME,
        DB_PASSWORD=DB_PASSWORD,
        DB_CONNECTION=DB_CONNECTION
    )
    
and the DAL URI is:

    db = DAL(settings.GAE_DB_URI,
         migrate_enabled=False, # Used to create tables by hand.
         pool_size=settings.DB_POOL_SIZE)

Above, the all-important detail is `migrate_enabled=False`. 

#### Creating the database tables

Normally, PyDAL keeps track of the state of the database tables, and when you change their definitions or define new tables, it modifies the database accordingly. 
This works very well for the testing database.  
For that, the files of the form `databases/*.table` reflect what (PyDAL thinks) is the current state of the tables, and new tables get created or removed or updated as needed. 
If you ever lose synch between the belief in `databases/*.table` and the reality, see the [documentation on migrations](http://www.web2py.com/books/default/chapter/29/06/the-database-abstraction-layer#table_migrations) and [migrate](http://www.web2py.com/books/default/chapter/29/06/the-database-abstraction-layer#Default-migration-settings) .

For production databases, it is far wiser to do modifications by hand, very carefully. 
For that, you can look at the `sql.log` file that is created when you do modifications on the testing server, and then you can replay the modifications carefully by hand (or via a script) to the production server.  
For this app, the `sql.log` file contains these table creations:

    CREATE TABLE `py4web_session`(
        `id` INT AUTO_INCREMENT NOT NULL,
        `rkey` VARCHAR(512),
        `rvalue` LONGTEXT,
        `expiration` INTEGER,
        `created_on` DATETIME,
        `expires_on` DATETIME,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB CHARACTER SET utf8;

    CREATE TABLE `auth_user`(
        `id` INT AUTO_INCREMENT NOT NULL,
        `email` VARCHAR(512) UNIQUE,
        `password` VARCHAR(512),
        `first_name` VARCHAR(512),
        `last_name` VARCHAR(512),
        `sso_id` VARCHAR(512),
        `action_token` VARCHAR(512),
        `last_password_change` DATETIME,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB CHARACTER SET utf8;

    CREATE TABLE `auth_user_tag_groups`(
        `id` INT AUTO_INCREMENT NOT NULL,
        `path` VARCHAR(512),
        `record_id` INT  , INDEX `record_id__idx` (`record_id`), FOREIGN KEY (`record_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB CHARACTER SET utf8;

    CREATE TABLE `sample_table`(
        `id` INT AUTO_INCREMENT NOT NULL,
        `name` VARCHAR(512),
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB CHARACTER SET utf8;

Another way to get the information you need for creating tables is to log into the testing databases, and execute, for each table you intend to re-create in production, `SHOW CREATE TABLE <table>;`. 
This will tell you how you can recreate the table. 
You can access the databases by connecting to them via the Cloud Shell; the option is offered in the Cloud Console for Cloud SQL. 

### Activate Appengine

Here, the one irreversible thing to select is the region.  
You should not get any question asked, but if it asks you questions, reply with: 

* Python 3
* Standard environment (not flexible environment)

Background: 

* Standard environment enables you to concern yourself mostly with the Python code only. 
* Flexible environment requires a setup file that is more similar to how you specify a docker file.  

For comparison, see https://cloud.google.com/appengine/docs/the-appengine-environments

## Deploy on Appengine

### Prepare the app for deployment

You can find instructions in the `deployment_tools/gae/README.md` file in py4web, in case these instructions become obsolete, and also in `deployment_tools/gae/Makefile`. 

Follow first of all these instructions:

    cd deployment_tools/gae
    make setup
    mkdir apps
    touch apps/__init__.py
    # symlink the apps that you want to deploy to GAE, for example:
    cd apps
    ln -s ../../../apps/gcloud_start .
    cd ..
    
The last step puts into `apps` the applications you want to deploy to the cloud.

### Deploy your app

You can then deploy via: 

    gcloud app deploy -v myversion
    
This will show you a confirmation screen, and then perform the deployment. 
You may be asked to authenticate your identity with the google cloud via a 
browser. 
The `myversion` is optional; if you do not specify a version, the version is named after the timestamp.  This is great, except that if you do a lot of deployments, as it is normal in debugging, you will end up with a lot of versions. 

### Versions

Appengine apps are served by _versions_ that belong to _services_. 
Here we are considering only the default service, but you can route traffic among versions. 
To access a version `myversion`, you can use the URL:

    https://myversion-dot-myproject.appspot.com
    
For the full documentation on the range of options available, see the [documentation on Appengine request routing](https://cloud.google.com/appengine/docs/standard/python/how-requests-are-routed). 

Most importantly, you can define which version is used by default by incoming traffic, and you can split traffic according to percentages to different versions; see the documentation on [traffic management on Appengine](https://cloud.google.com/appengine/docs/standard/python3/splitting-traffic).

Note that you can also [buy a domain name](https://domains.google.com), and then [set up your domain for use with an SSL certificate](https://cloud.google.com/appengine/docs/standard/python/securing-custom-domains-with-ssl), without even having to pay for the certificate. 
The domain settings for your application can be managed from the [custom domains page](https://cloud.google.com/appengine/docs/standard/python/securing-custom-domains-with-ssl).
 

### Look at the logs

Look at the logs.  Unfortunately, the information, which once used to be contained in a single log, is now spread across different sub-log types, so be sure in the log selector to look at all logs that can be relevant.  You need to explore a bit. 
There are two types of events relevant here: 

* When a version is first loaded, you will see events connected to py4web starting up: any errors loading applications or setting up connections to the database, mainly. 
* When an HTTP access happens, there will be a log line. 

The logs are fully searchable. 






